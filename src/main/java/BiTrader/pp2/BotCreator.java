package BiTrader.pp2;

import org.gradle.internal.impldep.com.google.gson.Gson;

import bot.Bot;
import market.Market;
import operation.OrderCreator;
import wallet.Wallet;

public class BotCreator {
	private BotSetup setup;
	private Market market;
	private Wallet wallet;
	private Gson gson;
	private String configFileName;
	private Bot bot;
	
	
	public BotCreator(String configFileName, Market market) {
		this.gson = new Gson();
		this.configFileName = configFileName;
		this.market = market;
	}
	
	public BotSetup getBotSetup() {
		String configContent = JsonParser.parse(configFileName);
		return gson.fromJson(configContent, BotSetup.class);
	}
	
	private void setupConfigurations() {
		setup = getBotSetup();
		wallet = new Wallet(setup.getAmount());
	}

	public Bot createBot() {
		setupConfigurations();
		OrderCreator orderCreator = new OrderCreator(wallet);
		bot = new Bot(setup.getRules(), market, orderCreator, setup.getLoopTime());
		return bot;
	}
}
