package BiTrader.pp2;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

import UI.pp2.MarketSelectorView;
import bot.Bot;
import market.NullMarket;

public class BiTrader {
	public static void main(String[] args) throws MalformedURLException, FileNotFoundException {
		MarketFinder marketFinder = new MarketFinder("plugins");
		marketFinder.findNames();
		BotCreator creator = new BotCreator("rules.json", new NullMarket());
		Bot bot = creator.createBot();
		MarketSelector marketSelector = new MarketSelector(bot, marketFinder);
		MarketSelectorView marketSelectorView = new MarketSelectorView(marketSelector);
		marketSelectorView.show();
		bot.run();
	}
}
