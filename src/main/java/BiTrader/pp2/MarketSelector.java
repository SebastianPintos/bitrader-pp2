package BiTrader.pp2;

import java.util.List;

import bot.Bot;
import market.Market;

public class MarketSelector {
	private MarketFinder marketFinder;
	private Bot bot;
	
	public MarketSelector(Bot bot, MarketFinder marketFinder) {
		this.marketFinder = marketFinder;
		this.bot = bot;
	}

	public void setMarket(String marketName) {
		Market market = marketFinder.find(marketName);
		bot.stopBot();
		bot.setMarket(market);
		bot.run();
	}
	
	public List<String> getMarketNames() {
		return this.marketFinder.getMarketNames();
	}
}
