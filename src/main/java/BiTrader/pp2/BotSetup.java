package BiTrader.pp2;
import java.util.List;

import rules.Rule;

public class BotSetup {
	private List<Rule> rules;
	private int loopTime;
	private double amount;
	
	public BotSetup(List<Rule> rules, int loopTime, double amount) {
		this.rules = rules;
		this.loopTime = loopTime;
		this.amount = amount;
	}
	public List<Rule> getRules() {
		return rules;
	}

	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}

	public int getLoopTime() {
		return loopTime;
	}

	public void setLoopTime(int loopTime) {
		this.loopTime = loopTime;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
