package BiTrader.pp2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class JsonParser {

	public static String parse(String path) {
		return usingBufferedReader(path);
	}

	private static String usingBufferedReader(String filePath) {
		StringBuilder contentBuilder = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {

			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				contentBuilder.append(sCurrentLine).append("\n");
			}
		} catch (IOException e) {
		}
		return contentBuilder.toString();
	}
}
