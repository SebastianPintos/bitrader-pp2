package BiTrader.pp2;

import market.Market;
import market.NullMarket;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class MarketFinder {
    private List<String> marketNames;
    private String path;

    public MarketFinder(String path) {
        this.marketNames = new ArrayList<>();
        this.path = path;
        path = resolveAbsolutePath(path);
    }

    public Market find(String marketName) {
        Market market = new NullMarket();
        try {
            File file = new File(path);
            File[] files = file.listFiles();
            if (files.length == 0)
                return market;
            file = files[0];
            JarFile jarFile = new JarFile(file);

            Enumeration<JarEntry> entries = jarFile.entries();
            URLClassLoader clazzLoader = URLClassLoader.newInstance(new URL[] { file.toURI().toURL() });

            while (entries.hasMoreElements()) {
                JarEntry element = entries.nextElement();
                if (!element.getName().endsWith(".class"))
                    continue;
                String name = element.getName().replace("/", ".").replace(".class", "");
                if (!name.endsWith(marketName))
                    continue;
                jarFile.close();
                Class<?> cls = Class.forName(name, true, clazzLoader);
                if (!Market.class.isAssignableFrom(cls))
                    continue;
                return (Market) cls.getDeclaredConstructor().newInstance();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return market;
    }

    public List<String> findNames() {
        try {
            File file = new File(path);
            for (File f : file.listFiles()) {
                JarFile jarFile = new JarFile(f);
                Enumeration<JarEntry> entries = jarFile.entries();
                URLClassLoader clazzLoader = URLClassLoader.newInstance(new URL[] { f.toURI().toURL() });
                while (entries.hasMoreElements()) {
                    JarEntry element = entries.nextElement();
                    if (!element.getName().endsWith(".class"))
                        continue;
                    String name = element.getName().replace("/", ".").replace(".class", "");
                    Class<?> cls = Class.forName(name, true, clazzLoader);
                    if (!Market.class.isAssignableFrom(cls))
                        continue;
                    marketNames.add(name.substring(name.lastIndexOf(".") + 1));
                }
                jarFile.close();
            }

        } catch (Exception e) {
        }
        return marketNames;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<String> getMarketNames() {
        return marketNames;
    }

    private String resolveAbsolutePath(String path) {
        try {
            return new File("").getAbsolutePath().toString() + File.separator + path;
        } catch (Exception e) {
            return "";
        }
    }
}
