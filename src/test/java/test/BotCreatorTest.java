package test;

import BiTrader.pp2.BotCreator;
import BiTrader.pp2.BotSetup;
import market.NullMarket;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BotCreatorTest {
    BotCreator botCreator;

    @Before
    public void init(){
        String configFileName = "src/test/resources/rules.json";
        botCreator = new BotCreator(configFileName, new NullMarket());
    }

    @Test
    public void createBot() {
        assertNotNull(botCreator.createBot());
    }

    @Test
    public void getBotSetupTest(){
        BotSetup botSetup = botCreator.getBotSetup();
        assertNotNull(botSetup);
    }

}
