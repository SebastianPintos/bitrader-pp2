package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import BiTrader.pp2.JsonParser;


public class JsonParserTest {
	
	@Test
	public void testJsonParserParseFail() {
		assertEquals(JsonParser.parse("/asdasd"), "");
	}
	
	@Test
	public void testJsonParseParseOk() {
		assertNotNull(JsonParser.parse("rules.json"));
	}
	
	@Test
	public void testJsonParseOk() {
		assertNotNull(new JsonParser());
	}
}
