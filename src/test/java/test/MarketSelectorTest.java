package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import BiTrader.pp2.MarketFinder;
import org.junit.Before;
import org.junit.Test;

import BiTrader.pp2.BotCreator;
import BiTrader.pp2.MarketSelector;
import bot.Bot;
import market.NullMarket;

public class MarketSelectorTest {
	private MarketSelector marketSelector;
	private MarketFinder finder;
	private Bot bot;

	@Before
	public void init() {
		finder = new MarketFinder("src/test/resources/plugins");
		finder.findNames();
		BotCreator creator = new BotCreator("rules.json", new NullMarket());
		bot = creator.createBot();
		bot.stopBot();
		marketSelector = new MarketSelector(bot, finder);
	}

	@Test
	public void createMarketSelectorTest() {
		assertNotNull(marketSelector);
	}

	@Test
	public void setMarketTestFail() {
		marketSelector.setMarket("SomeMarket");
		assertNull(bot.getMarket().getName());
	}
	@Test
	public void setMarketTestOk() {
		marketSelector.setMarket("Rippio");
		assertEquals("RIPPIO", bot.getMarket().getName());
	}

	@Test
	public void getNameMarketsOk() {
		assertEquals(marketSelector.getMarketNames().size(), 2);
	}
}
