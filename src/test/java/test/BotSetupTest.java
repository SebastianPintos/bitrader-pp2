package test;


import BiTrader.pp2.BotSetup;
import cryptoCurrency.CryptoCurrency;
import operation.OperationType;
import org.junit.Before;
import org.junit.Test;
import rules.Rule;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BotSetupTest {
    BotSetup botSetup;

    @Before
    public void init(){
        List<Rule> rules = createRules();
        int loopTime = 100;
        double amount = 10000.0;
        botSetup = new BotSetup(rules,loopTime,amount);
    }

    @Test
    public void botSetupGetRules(){
        assertEquals(botSetup.getRules().size(),1);
    }
    @Test
    public void botSetupSetRules(){
        botSetup.setRules(new ArrayList<>());
        assertEquals(botSetup.getRules().size(),0);
    }
    @Test
    public void botSetupGetLoopTime(){
        assertEquals(botSetup.getLoopTime(),100);
    }
    @Test
    public void botSetupSetLoopTime(){
        botSetup.setLoopTime(200);
        assertEquals(botSetup.getLoopTime(),200);
    }
    @Test
    public void botSetupGetAmount(){
        assertEquals(botSetup.getAmount(),10000.0,0);
    }
    @Test
    public void botSetupSetAmount(){
        botSetup.setAmount(20000.0);
        assertEquals(botSetup.getAmount(),20000.0,0);
    }
    private List<Rule> createRules() {
        List<Rule> rules = new ArrayList<>();
        double deltaPercentage = 5.0;
        CryptoCurrency cryptoCurrency = new CryptoCurrency("BITCOIN","BTC");
        double quantity = 1.0;
        OperationType operationType = OperationType.BUY;
        double referencePrice = 100.0;
        rules.add(new Rule(deltaPercentage,cryptoCurrency,quantity,operationType,referencePrice));
        return rules;
    }
}
