package criteriosAceptacion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import BiTrader.pp2.BotCreator;
import BiTrader.pp2.MarketFinder;
import BiTrader.pp2.MarketSelector;
import bot.Bot;
import market.NullMarket;

public class Story4 {
	private MarketSelector marketSelector;
	private MarketFinder finder;
	private Bot bot;

	@Before
	public void init() {
		finder = new MarketFinder("src/test/resources/plugins");
		finder.findNames();
		BotCreator creator = new BotCreator("rules.json", new NullMarket());
		bot = creator.createBot();
		bot.stopBot();
		marketSelector = new MarketSelector(bot, finder);
	}

	@Test
	public void CA1() {
		List<String> markets = marketSelector.getMarketNames();
		assertTrue(markets.contains("Rippio"));
		assertTrue(markets.contains("Binance"));
	}

	@Test
	public void CA2() {
		marketSelector.setMarket("Binance");
		assertEquals("BINANCE", bot.getMarketName());
	}}
